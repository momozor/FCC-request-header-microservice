(defsystem "request-header-microservice-test"
  :defsystem-depends-on ("prove-asdf")
  :author ""
  :license ""
  :depends-on ("request-header-microservice"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "request-header-microservice"))))
  :description "Test system for request-header-microservice"
  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
