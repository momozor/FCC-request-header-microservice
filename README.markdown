# request-header-microservice
My FreeCodeCamp request-header-microservice exercise solution in Common Lisp (caveman2).

## Usage
* `(ql:quickload :request-header-microservice)`
* Launch a local web development server ***hunchentoot*** at port 3000- `(request-header-microservice:start :port 3000)`
* Launch and navigate your web browser to http://localhost:3000
* Enjoy! :)

## User Story
1. I can get the IP address, preferred languages (from header Accept-Language)
and system infos (from header User-Agent) for my device.

## Example API Usage
* GET http://localhost:3000/api/whoami

## Installation
1. `git clone https://github.com/momozor/FCC-request-header-microservice`
2. Load the project with Quicklisp - `(ql:quickload :request-header-microservice)`

## Authors
* momozor

## License
This project is licensed under the MIT license. See LICENSE file for more details.
