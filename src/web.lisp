(in-package :cl-user)
(defpackage request-header-microservice.web
  (:use :cl
        :caveman2
        :request-header-microservice.config
        :request-header-microservice.view
        :request-header-microservice.db
        :datafly
        :sxql
        :alexandria)
  (:export :*web*))
(in-package :request-header-microservice.web)


;;
;; Application

(defclass <web> (<app>) ())
(defvar *web* (make-instance '<web>))
(clear-routing-rules *web*)

;;
;; Routing rules

(defroute "/" ()
  (render #P"index.html"))

;; see timestamp-microservice.view for *request* and request-*
(defroute "/api/whoami" ()
  (let* ((ipaddress (request-remote-addr *request*))
         (language (gethash "accept-language" (request-headers *request*)))
         (software (gethash "user-agent" (request-headers *request*))))
    (render-json `(ipaddress ,ipaddress language ,language software ,software))))

;;
;; Error pages

(defmethod on-exception ((app <web>) (code (eql 404)))
  (declare (ignore app))
  (merge-pathnames #P"_errors/404.html"
                   *template-directory*))
