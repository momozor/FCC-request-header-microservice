(in-package :cl-user)
(defpackage request-header-microservice
  (:use :cl)
  (:import-from :request-header-microservice.config
                :config)
  (:import-from :clack
                :clackup)
  (:export :start
           :stop))
(in-package :request-header-microservice)

(defvar *appfile-path*
  (asdf:system-relative-pathname :request-header-microservice #P"app.lisp"))

(defvar *handler* nil)

(defun start (&rest args &key server port debug &allow-other-keys)
  (declare (ignore server port debug))
  (when *handler*
    (restart-case (error "Server is already running.")
      (restart-server ()
        :report "Restart the server"
        (stop))))
  (setf *handler*
        (apply #'clackup *appfile-path* args)))

(defun stop ()
  (prog1
      (clack:stop *handler*)
    (setf *handler* nil)))
